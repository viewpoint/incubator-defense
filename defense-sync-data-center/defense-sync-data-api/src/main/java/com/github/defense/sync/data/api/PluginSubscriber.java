package com.github.defense.sync.data.api;

import com.github.defense.common.dto.Plugin;

/**
 * 插件的启用开关
 * @Author: lettger
 * @Date: 2021/11/30 上午10:50
 */
public interface PluginSubscriber {

    /**
     * On subscribe
     * @param plugin
     */
    default void  onSubscribe(Plugin plugin){}

    /**
     * un subscribe
     * @param plugin
     */
    default void unSubscribe(Plugin plugin){

    }

    /**
     * 刷新所有插件
     */
    default void refreshAll(){}
}
