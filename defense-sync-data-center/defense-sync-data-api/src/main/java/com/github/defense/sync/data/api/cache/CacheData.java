package com.github.defense.sync.data.api.cache;

import com.github.defense.common.dto.App;
import com.github.defense.common.dto.Gateway;
import com.github.defense.common.dto.Plugin;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 所有的本地缓存数据
 * @Author: lettger
 * @Date: 2021/11/23 下午1:32
 */
public final class CacheData {

    private static final CacheData INSTANCE = new CacheData();

    private static final ConcurrentMap<String, Plugin> PLUGIN_MAP = new ConcurrentHashMap();
    private static final ConcurrentMap<String, Gateway> GATEWAY_MAP = new ConcurrentHashMap();
    private static final ConcurrentMap<String, App> APP_MAP = new ConcurrentHashMap();


    private CacheData() {
    }

    public static CacheData getInstance() {
        return INSTANCE;
    }

    /**
     * 缓存中获取插件
     * @param name
     * @return
     */
    public Plugin obtainPlugin(final String name){
        return PLUGIN_MAP.get(name);
    }

    /**
     * 缓存插件
     * @param plugin
     */
    public void cachePlugin(final Plugin plugin) {
        Optional.ofNullable(plugin).ifPresent(data -> PLUGIN_MAP.put(data.getName(), data));
    }

    /**
     * 移除缓存插件
     * @param plugin
     */
    public void removePlugin(final Plugin plugin) {
        Optional.ofNullable(plugin).ifPresent(data -> PLUGIN_MAP.remove(data.getName()));
    }

    /**
     * 清楚缓存数据
     */
    public void cleanPlugin(){
        PLUGIN_MAP.clear();
    }
}
