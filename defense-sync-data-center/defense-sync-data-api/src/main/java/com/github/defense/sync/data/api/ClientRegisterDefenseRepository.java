package com.github.defense.sync.data.api;

/**
 * 客户端注册 defense admin
 * @Author: lettger
 * @Date: 2021/12/2 下午2:25
 */
public interface ClientRegisterDefenseRepository {

    /**
     * 向defense-admin注册数据
     * @param
     */
    <T> void register(T t);

}
