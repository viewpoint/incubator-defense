package com.github.defense.plugin.gray.instance;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 服务实例集合
 * @Author: lettger
 * @Date: 2021/6/18 1:04 下午
 */
@Data
@NoArgsConstructor
public class GrayInstanceList<S> {
    /**
     * 灰度实例
     */
    private List<S> grayInstance;
    /**
     * 正常实例
     */
    private List<S> normalInstance;

    public GrayInstanceList(List<S> grayInstance, List<S> normalInstance) {
        this.grayInstance = grayInstance;
        this.normalInstance = normalInstance;
    }
}
