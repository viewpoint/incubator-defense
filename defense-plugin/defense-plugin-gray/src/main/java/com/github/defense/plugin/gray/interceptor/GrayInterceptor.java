package com.github.defense.plugin.gray.interceptor;

import com.github.defense.plugin.gray.enums.GrayType;
import com.github.defense.plugin.gray.request.GrayRequest;
import org.springframework.core.Ordered;

/**
 * 自定义灰度业务拦截器
 *
 * @Author: lettger
 * @Date: 2021/6/6 8:50 下午
 */
public interface GrayInterceptor extends Ordered {

    /**
     * 拦截器业务类型
     *
     * @return
     */
    GrayType getGrayType();

    /**
     * 是否应该被执行
     *
     * @return
     */
    boolean isShould();

    /**
     * 处理之前
     *
     * @param request
     * @return
     */
    boolean preHandler(GrayRequest request);

    /**
     * 处理之后
     *
     * @param request
     * @return
     */
    boolean afterHandler(GrayRequest request);

    /**
     * 排序
     * @return
     */
    @Override
    default int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

}
