package com.github.defense.plugin.gray.context;

import com.github.defense.plugin.gray.request.GrayRequest;

/**
 * 灰度上下文
 *
 * @Author: lettger
 * @Date: 2021/6/18 8:42 下午
 */
public interface GrayLocalContext {

    /**
     * 设置灰度请求
     *
     * @param request
     */
    void setGrayRequest(GrayRequest request);

    /**
     * 获取灰度请求信息
     *
     * @return
     */
    GrayRequest getGrayRequest();

    /**
     * 移除灰度信息
     */
    void removeLocalContext();
}
