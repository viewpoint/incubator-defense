package com.github.defense.plugin.gray.decision.predicate;

import com.github.defense.plugin.gray.decision.GrayDecision;
import com.github.defense.plugin.gray.instance.GrayInstance;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 灰度决策信息
 * @Author: lettger
 * @Date: 2021/6/23 10:11 上午
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PolicyDefinition {
    /**
     * 决策者
     */
    private GrayDecision grayDecision;
    /**
     * 实例信息
     */
    private GrayInstance instance;
}
