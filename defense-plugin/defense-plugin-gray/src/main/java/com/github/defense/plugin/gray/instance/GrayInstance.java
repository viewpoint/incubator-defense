package com.github.defense.plugin.gray.instance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * 灰度实例信息
 * @Author: lettger
 * @Date: 2021/6/17 6:41 下午
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GrayInstance<S> {
    /**
     * 实例ID
     */
    private String instanceId;

    /**
     * 服务ID
     */
    private String serviceId;

    /**
     * 请求地址
     */
    private URI uri;

    /**
     * 元数据
     */
    private Map<String,Object> metadata = new HashMap<>();

    /**
     * 服务
     */
    private S server;
}
