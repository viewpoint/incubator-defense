package com.github.defense.plugin.gray.configuration;

import com.alibaba.cloud.nacos.ribbon.NacosServerIntrospector;
import com.github.defense.plugin.gray.server.NacosServerExplainer;
import com.github.defense.plugin.gray.server.ServerExplainer;
import com.netflix.loadbalancer.Server;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: lettger
 * @Date: 2021/6/11 9:23 上午
 */
@Configuration
public class GrayNacosAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public NacosServerIntrospector nacosServerIntrospector() {
        return new NacosServerIntrospector();
    }

    @Bean
    @ConditionalOnMissingBean(ServerExplainer.class)
    public ServerExplainer<Server> serverExplainer(NacosServerIntrospector introspector) {
        return new NacosServerExplainer(introspector);
    }
}
