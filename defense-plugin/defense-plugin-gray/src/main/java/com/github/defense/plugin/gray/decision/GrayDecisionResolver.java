package com.github.defense.plugin.gray.decision;

import com.github.defense.plugin.gray.decision.factory.GrayDecisionFactory;
import com.github.defense.plugin.gray.decision.predicate.DecisionDefinition;
import com.github.defense.plugin.gray.decision.predicate.PolicyDefinition;

/**
 * GrayDecision 分解器
 * @Author: lettger
 * @Date: 2021/6/8 6:27 下午
 */
public interface GrayDecisionResolver {

    /**
     * 获取策略工厂
     * @param name
     * @return
     */
    GrayDecisionFactory getFactory(String name);

    /**
     * 获取决策信息
     * @param definition
     * @return
     */
    PolicyDefinition getPolicyDefinition(DecisionDefinition definition);
}
