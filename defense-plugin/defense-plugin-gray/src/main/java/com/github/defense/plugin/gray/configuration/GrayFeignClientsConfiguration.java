package com.github.defense.plugin.gray.configuration;

import com.github.defense.plugin.gray.context.RoutingHandler;
import com.github.defense.plugin.gray.netflix.feign.FeignGrayInterceptor;
import com.github.defense.plugin.gray.netflix.feign.FeignRequestInterceptor;
import com.github.defense.plugin.gray.netflix.feign.GrayFeignClient;
import feign.Client;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: lettger
 * @Date: 2021/6/20 9:51 下午
 */
@Configuration
public class GrayFeignClientsConfiguration {


    @Bean
    public FeignGrayInterceptor feignGrayInterceptor(){
        return new FeignGrayInterceptor();
    }

    @Bean
    public FeignRequestInterceptor feignRequestInterceptor(){
        return new FeignRequestInterceptor();
    }

    @Bean
    public Client grayFeignClient(Client client, RoutingHandler handler){
        return new GrayFeignClient(client,handler);
    }

}
