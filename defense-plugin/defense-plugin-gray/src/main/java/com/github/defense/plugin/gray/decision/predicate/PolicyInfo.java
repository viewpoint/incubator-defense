package com.github.defense.plugin.gray.decision.predicate;

import com.github.defense.plugin.gray.instance.GrayInstance;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: lettger
 * @Date: 2021/6/23 10:03 上午
 */
@Data
public class PolicyInfo {

    private GrayInstance instance;

    private Map<String, HashMap> metadata;
}
