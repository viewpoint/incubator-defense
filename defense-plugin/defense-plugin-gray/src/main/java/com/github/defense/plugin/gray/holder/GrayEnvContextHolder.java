package com.github.defense.plugin.gray.holder;

import lombok.extern.slf4j.Slf4j;

/**
 * 环境变量上下文信息
 * @Author: lettger
 * @Date: 2021/6/17 4:09 下午
 */
@Slf4j
public class GrayEnvContextHolder {

    /**
     * 校验Boolean 类型是否存在
     * @param key 环境key
     * @return
     */
    public static boolean getEnvBool(String key){
        String result =  SpringContextHolder.getEnvironment().getProperty(key);
        return Boolean.parseBoolean(result);
    }

    /**
     * 获取字符串类型
     * @param key
     * @return
     */
    public static String getEnvStr(String key){
        return SpringContextHolder.getEnvironment().getProperty(key);
    }
}
