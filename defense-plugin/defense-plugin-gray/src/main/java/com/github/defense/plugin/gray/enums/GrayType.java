package com.github.defense.plugin.gray.enums;

/**
 * 灰度参与者类型
 * 有这么多组件参与灰度
 * @Author: lettger
 * @Date: 2021/6/7 12:51 下午
 */
public enum GrayType {

    ZUUL,
    GATEWAY,
    RIBBON,
    REST,
    FEIGN,
    HYSTRIX;

}
