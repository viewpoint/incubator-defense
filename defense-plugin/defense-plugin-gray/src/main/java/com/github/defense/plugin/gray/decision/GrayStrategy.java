package com.github.defense.plugin.gray.decision;


import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * 灰度策略判断
 *
 * @Author: lettger
 * @Date: 2021/6/8 3:04 下午
 */
public enum GrayStrategy {

    /**
     * 只要有一个条件满足即返回true,默认走这个
     */
    ANY {
        @Override
        public boolean compare(Object source, String target) {
            try {
                if (Objects.isNull(source) || Objects.isNull(target)) {
                    return false;
                }
                Collection<String> collection = Lists.newArrayList();
                if (source instanceof Collection) {
                    List<String> src = new ArrayList<>((Collection<String>) source);
                    collection.add(src.get(0));
                } else {
                    collection.add(source.toString());
                }
                return collection.stream().anyMatch(value -> value.matches(target));
            } catch (Exception e) {
                return false;
            }
        }
    },
    /**
     * 必须全部满足才会返回true
     */
    ALL {
        @Override
        public boolean compare(Object source, String target) {
            try {
                if (Objects.isNull(source) || Objects.isNull(target)) {
                    return false;
                }
                Collection<String> collection = Lists.newArrayList();
                if (source instanceof Collection) {
                    List<String> src = new ArrayList<>((Collection<String>) source);
                    collection.add(src.get(0));
                } else {
                    collection.add(source.toString());
                }
                return collection.stream().allMatch(value -> value.matches(target));
            } catch (Exception e) {
                return false;
            }
        }
    },
    /**
     * 全部不满足才会返回true
     */
    NONE {
        @Override
        public boolean compare(Object source, String target) {
            try {
                if (Objects.isNull(source) || Objects.isNull(target)) {
                    return false;
                }
                Collection<String> collection = Lists.newArrayList();
                if (source instanceof Collection) {
                    List<String> src = new ArrayList<>((Collection<String>) source);
                    collection.add(src.get(0));
                } else {
                    collection.add(source.toString());
                }
                return collection.stream().noneMatch(value -> value.matches(target));
            } catch (Exception e) {
                return false;
            }
        }
    },

    /**
     * 比较两个值相等
     */
    EQ {
        @Override
        public boolean compare(Object source, String target) {
            try {
                if (Objects.isNull(source) || Objects.isNull(target)) {
                    return false;
                }
                if (source instanceof Collection) {
                    List<String> src = new ArrayList<>((Collection<String>) source);
                    source = src.get(0);
                }
                if (StringUtils.isNumeric(source.toString()) && StringUtils.isNumeric(target)) {
                    return Integer.parseInt(source.toString()) == Integer.parseInt(target);
                }
                return Objects.equals(source.toString(), target);
            } catch (Exception e) {
                return false;
            }
        }
    },
    /**
     * 两个值不等
     */
    NEQ {
        @Override
        public boolean compare(Object source, String target) {
            try {
                if (Objects.isNull(source) || Objects.isNull(target)) {
                    return false;
                }
                if (source instanceof Collection) {
                    List<String> src = new ArrayList<>((Collection<String>) source);
                    source = src.get(0);
                }
                if (StringUtils.isNumeric(source.toString()) && StringUtils.isNumeric(target)) {
                    return Integer.parseInt(source.toString()) != Integer.parseInt(target);
                }
                return !Objects.equals(source.toString(), target);
            } catch (Exception e) {
                return false;
            }
        }
    },
    /**
     * 数字比较 a > b
     */
    GT {
        @Override
        public boolean compare(Object source, String target) {
            try {
                if (Objects.isNull(source) || Objects.isNull(target)) {
                    return false;
                }
                if (source instanceof Collection) {
                    List<String> src = new ArrayList<>((Collection<String>) source);
                    source = src.get(0);
                }
                if (StringUtils.isNumeric(source.toString()) && StringUtils.isNumeric(target)) {
                    return Integer.parseInt(source.toString()) > Integer.parseInt(target);
                }
            } catch (Exception e) {

            }
            return false;
        }
    },
    /**
     * 数字比较 a >= b
     */
    GTE {
        @Override
        public boolean compare(Object source, String target) {
            try {
                if (Objects.isNull(source) || Objects.isNull(target)) {
                    return false;
                }
                if (source instanceof Collection) {
                    List<String> src = new ArrayList<>((Collection<String>) source);
                    source = src.get(0);
                }
                if (StringUtils.isNumeric(source.toString()) && StringUtils.isNumeric(target)) {
                    return Integer.parseInt(source.toString()) >= Integer.parseInt(target);
                }
            } catch (Exception e) {

            }
            return false;
        }
    },
    /**
     * 数字比较 a < b
     */
    LT {
        @Override
        public boolean compare(Object source, String target) {
            try {
                if (Objects.isNull(source) || Objects.isNull(target)) {
                    return false;
                }
                if (source instanceof Collection) {
                    List<String> src = new ArrayList<>((Collection<String>) source);
                    source = src.get(0);
                }
                if (StringUtils.isNumeric(source.toString()) && StringUtils.isNumeric(target)) {
                    return Integer.parseInt(source.toString()) < Integer.parseInt(target);
                }
            } catch (Exception e) {

            }
            return false;
        }
    },
    /**
     * 数字比较 a <= b
     */
    LTE {
        @Override
        public boolean compare(Object source, String target) {
            try {
                if (Objects.isNull(source) || Objects.isNull(target)) {
                    return false;
                }
                if (source instanceof Collection) {
                    List<String> src = new ArrayList<>((Collection<String>) source);
                    source = src.get(0);
                }
                if (StringUtils.isNumeric(source.toString()) && StringUtils.isNumeric(target)) {
                    return Integer.parseInt(source.toString()) <= Integer.parseInt(target);
                }
            } catch (Exception e) {

            }
            return false;
        }
    },
    ;

    public abstract boolean compare(Object source, String target);

}
