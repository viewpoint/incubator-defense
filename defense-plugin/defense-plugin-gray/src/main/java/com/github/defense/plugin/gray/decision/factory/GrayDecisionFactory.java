package com.github.defense.plugin.gray.decision.factory;

import com.github.defense.plugin.gray.decision.GrayDecision;
import com.github.defense.plugin.gray.utils.NameUtils;

import java.util.function.Consumer;

/**
 * 灰度决策工厂
 * @Author: lettger
 * @Date: 2021/6/8 11:20 上午
 */
public interface GrayDecisionFactory<Config> {

    /**
     * 获取类前缀名称
     * @return
     */
    default String name(){
        return NameUtils.normalizeName(getClass(),GrayDecisionFactory.class);
    }

    default Config newConfig(){
        throw new UnsupportedOperationException("newConfig() not implemented");
    }


    default GrayDecision apply(Consumer<Config> consumer) {
        Config config = newConfig();
        consumer.accept(config);
        return apply(config);
    }

    GrayDecision apply(Config config);
}
