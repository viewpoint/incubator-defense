package com.github.defense.plugin.gray;

import com.github.defense.plugin.gray.holder.SpringContextHolder;
import com.github.defense.plugin.gray.choose.GrayServerChoose;
import com.github.defense.plugin.gray.holder.GrayClientHolder;
import com.github.defense.plugin.gray.interceptor.GrayInterceptor;
import com.github.defense.plugin.gray.manager.GrayManager;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

/**
 * @Author: lettger
 * @Date: 2021/6/7 2:39 下午
 */
public class GrayClientInitializer implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        GrayClientHolder.setManager(SpringContextHolder.getBean(GrayManager.class));
        // 初始化自定义灰度拦截器
        Map<String, GrayInterceptor> interceptorMap = SpringContextHolder.getBeansOfType(GrayInterceptor.class);
        GrayClientHolder.getManager().setGrayInterceptors(interceptorMap.values());
        GrayClientHolder.setServerChoose(SpringContextHolder.getBean(GrayServerChoose.class));
    }
}
