package com.github.defense.plugin.gray.configuration;

import com.github.defense.plugin.gray.netflix.ribbon.GrayRibbonRule;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.ZoneAvoidanceRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

/**
 * @Author: lettger
 * @Date: 2021/6/16 12:49 下午
 */
public class GrayRibbonConfiguration {

    @Bean
    public IRule ribbonRule(@Autowired(required = false) IClientConfig config) {
        ZoneAvoidanceRule zoneAvoidanceRule = new GrayRibbonRule();
        zoneAvoidanceRule.initWithNiwsConfig(config);
        return zoneAvoidanceRule;
    }
}
