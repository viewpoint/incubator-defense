package com.github.defense.plugin.gray.manager;

import com.github.defense.plugin.gray.decision.predicate.DecisionDefinition;
import com.github.defense.plugin.gray.decision.predicate.PolicyDefinition;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 灰度决策管理器
 * @Author: lettger
 * @Date: 2021/6/22 1:02 下午
 */
public interface GrayDecisionManager {
    /**
     * 获取决策者
     * @param definition
     * @return
     */
    PolicyDefinition getDefinition(DecisionDefinition definition);

    /**
     * 获取决策者集合
     * @param definitions
     * @return
     */
    default List<PolicyDefinition> getPolicies(Collection<DecisionDefinition> definitions){
        return definitions.stream()
                .map(this::getDefinition)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
