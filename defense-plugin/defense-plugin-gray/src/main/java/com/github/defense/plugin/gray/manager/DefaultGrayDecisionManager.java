package com.github.defense.plugin.gray.manager;

import com.github.defense.plugin.gray.decision.GrayDecisionResolver;
import com.github.defense.plugin.gray.decision.predicate.DecisionDefinition;
import com.github.defense.plugin.gray.decision.predicate.PolicyDefinition;

/**
 * 默认灰度决策管理器
 * @Author: lettger
 * @Date: 2021/6/22 1:18 下午
 */
public class DefaultGrayDecisionManager implements GrayDecisionManager {

    private GrayDecisionResolver decisionResolver;

    public DefaultGrayDecisionManager(GrayDecisionResolver decisionResolver) {
        this.decisionResolver = decisionResolver;
    }

    @Override
    public PolicyDefinition getDefinition(DecisionDefinition definition) {
        return decisionResolver.getPolicyDefinition(definition);
    }
}
