package com.github.defense.plugin.gray.server;


import com.github.defense.plugin.gray.instance.GrayInstance;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Server解释器
 * @Author: lettger
 * @Date: 2021/6/10 8:24 下午
 */
public interface ServerExplainer<S> {

    /**
     * 获取指定服务的元数据
     * @param s
     * @return
     */
    Map getMetaData(S s);

    /**
     * 获取服务的Id
     * @param s
     * @return
     */
    String getServiceId(S s);

    /**
     * 获取服务的实例ID
     * @param s
     * @return
     */
    String getInstateId(S s);

    /**
     * 将服务转成灰度实例{@link GrayInstance}
     * @param s
     * @return
     */
    default GrayInstance apply(S s) {
        return GrayInstance.<S>builder()
                .server(s)
                .instanceId(getInstateId(s))
                .serviceId(getServiceId(s))
                .metadata(getMetaData(s))
                .build();
    }

    /**
     * 将服务转成灰度实例{@link GrayInstance}
     * @param list
     * @return
     */
    default List<GrayInstance> applys(List<S> list){
        return list.stream().map(this::apply).collect(Collectors.toList());
    }
}
