package com.github.defense.plugin.gray.decision.factory;

import com.github.defense.plugin.gray.decision.GrayStrategy;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author: lettger
 * @Date: 2021/6/23 11:06 上午
 */
public abstract class AbstractGrayStrategyFactory<C extends AbstractGrayStrategyFactory.Strategy> extends AbstractGrayDecisionFactory<C> {


    public AbstractGrayStrategyFactory(Class<C> cClass) {
        super(cClass);
    }

    @Setter
    @Getter
    public static class Strategy {

        private GrayStrategy strategy = GrayStrategy.ANY;

    }
}
