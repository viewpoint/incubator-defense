package com.github.defense.plugin.gray.netflix.rest;

import com.github.defense.plugin.gray.enums.GrayType;
import com.github.defense.plugin.gray.interceptor.GrayInterceptor;
import com.github.defense.plugin.gray.request.GrayRequest;

/**
 * @Author: lettger
 * @Date: 2021/6/19 6:27 下午
 */
public class RestGrayInterceptor implements GrayInterceptor {

    @Override
    public GrayType getGrayType() {
        return GrayType.REST;
    }

    @Override
    public boolean isShould() {
        return true;
    }

    @Override
    public boolean preHandler(GrayRequest request) {
        return true;
    }

    @Override
    public boolean afterHandler(GrayRequest request) {
        return true;
    }
}
