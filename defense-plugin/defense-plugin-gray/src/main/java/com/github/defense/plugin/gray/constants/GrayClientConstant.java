package com.github.defense.plugin.gray.constants;

/**
 * 灰度客户端常量
 * @Author: lettger
 * @Date: 2021/6/5 1:41 下午
 */
public interface GrayClientConstant extends GrayConstant {

    /**
     * 启用灰度开关
     */
    String GRAY_CLIENT_ENABLE = "defense.gray.enabled";

    /**
     * 实例是否开启灰度
     */
    String GRAY_INSTANCE_ENABLE = "defense.gray.instance.enable";
}
