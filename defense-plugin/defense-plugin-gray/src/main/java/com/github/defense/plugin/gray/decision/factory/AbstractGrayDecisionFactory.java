package com.github.defense.plugin.gray.decision.factory;

import org.springframework.beans.BeanUtils;

/**
 * @Author: lettger
 * @Date: 2021/6/8 11:28 上午
 */
public abstract class AbstractGrayDecisionFactory<Config> implements GrayDecisionFactory<Config> {

    private Class<Config> configClass;

    protected AbstractGrayDecisionFactory(Class<Config> configClass) {
        this.configClass = configClass;
    }

    public Class<Config> getConfigClass() {
        return configClass;
    }

    @Override
    public Config newConfig() {
        return BeanUtils.instantiateClass(getConfigClass());
    }
}
