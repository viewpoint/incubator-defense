package com.github.defense.plugin.gray.choose;

import java.util.List;

/**
 *  服务筛选器
 *
 * @Author: lettger
 * @Date: 2021/6/9 12:47 下午
 */
public interface GrayServerChoose<S> {

    /**
     * 服务筛选
     * @param list
     * @param selection
     * @return
     */
    S choose(List<S> list,ServerSelection<S> selection);
}
