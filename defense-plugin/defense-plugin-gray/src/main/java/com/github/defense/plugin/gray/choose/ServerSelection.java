package com.github.defense.plugin.gray.choose;

import java.util.List;

/**
 * 服务筛选处理
 * @Author: lettger
 * @Date: 2021/6/21 6:28 下午
 */
public interface ServerSelection<S> {
    /**
     * 服务筛选处理
     * @param servers
     * @return
     */
    S choose(List<S> servers);
}
