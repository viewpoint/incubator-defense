package com.github.defense.plugin.gray.holder;


import com.github.defense.plugin.gray.choose.GrayServerChoose;
import com.github.defense.plugin.gray.manager.GrayManager;

/**
 * @Author: lettger
 * @Date: 2021/6/7 3:07 下午
 */
public class GrayClientHolder {

    private static GrayManager manager;
    private static GrayServerChoose<?> serverChoose;

    public static GrayManager getManager() {
        return manager;
    }

    public static void setManager(GrayManager manager) {
        GrayClientHolder.manager = manager;
    }

    public static <S> GrayServerChoose<S> getServerChoose() {
        return (GrayServerChoose<S>) serverChoose;
    }

    public static void setServerChoose(GrayServerChoose<?> serverChoose) {
        GrayClientHolder.serverChoose = serverChoose;
    }
}
