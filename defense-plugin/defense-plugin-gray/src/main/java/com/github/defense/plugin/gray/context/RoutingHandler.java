package com.github.defense.plugin.gray.context;

import com.github.defense.common.function.Supplier;

/**
 * 路由连接器 用来将zuul和gateway过滤器的路由信息放入到本地线程中去
 *
 * @Author: lettger
 * @Date: 2021/6/7 1:30 下午
 */
public interface RoutingHandler {

    /**
     * 执行网关连接
     *
     * @param context
     */
    void execute(RoutingLocalContext context);

    /**
     * 销毁本地线程
     *
     * @param context
     */
    void destroy(RoutingLocalContext context);

    /**
     * 执行RestTemplate连接
     *
     * @param context
     * @param supplier
     * @param <T>
     * @return
     */
    default <T> T execute(RoutingLocalContext context, Supplier<T> supplier) {
        try {
            execute(context);
            return supplier.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            destroy(context);
        }
    }
}
