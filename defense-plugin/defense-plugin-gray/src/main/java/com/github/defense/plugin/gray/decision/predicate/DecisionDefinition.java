package com.github.defense.plugin.gray.decision.predicate;

import com.github.defense.plugin.gray.instance.GrayInstance;
import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 灰度决策 具体的规则
 * @Author: lettger
 * @Date: 2021/6/21 5:14 下午
 */
@Data
public class DecisionDefinition implements Serializable {
    /**
     * 决策名称 GrayDecisionFactory XXXGrayDecisionFactory对应的类前缀名称
     */
    private String name;
    /**
     * 决策信息 其实就是判断条件 对应GrayDecisionFactory中的Config
     */
    private Map<String, String> infos = new LinkedHashMap<>();
    /**
     * 实例信息
     */
    private GrayInstance instance;
}
