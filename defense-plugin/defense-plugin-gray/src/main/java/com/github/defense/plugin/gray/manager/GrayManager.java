package com.github.defense.plugin.gray.manager;


import com.github.defense.plugin.gray.enums.GrayType;
import com.github.defense.plugin.gray.interceptor.GrayInterceptor;

import java.util.Collection;
import java.util.List;

/**
 * 灰度管理器
 * @Author: lettger
 * @Date: 2021/6/7 2:53 下午
 */
public interface GrayManager {

    /**
     * 获取自定义拦截器
     * @param grayType
     * @return
     */
    List<GrayInterceptor> getGrayInterceptor(GrayType grayType);

    /**
     * 设置拦截器
     * @param grayInterceptors
     */
    void setGrayInterceptors(Collection<GrayInterceptor> grayInterceptors);

}
