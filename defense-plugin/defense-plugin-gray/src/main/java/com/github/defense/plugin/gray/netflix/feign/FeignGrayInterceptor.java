package com.github.defense.plugin.gray.netflix.feign;

import com.github.defense.plugin.gray.enums.GrayType;
import com.github.defense.plugin.gray.interceptor.GrayInterceptor;
import com.github.defense.plugin.gray.request.GrayRequest;

/**
 * @Author: lettger
 * @Date: 2021/6/20 6:52 上午
 */
public class FeignGrayInterceptor implements GrayInterceptor {
    @Override
    public GrayType getGrayType() {
        return GrayType.FEIGN;
    }

    @Override
    public boolean isShould() {
        return true;
    }

    @Override
    public boolean preHandler(GrayRequest request) {
        return true;
    }

    @Override
    public boolean afterHandler(GrayRequest request) {
        return true;
    }
}
