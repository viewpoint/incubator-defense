package com.github.defense.plugin.gray.constants;

/**
 * @Author: lettger
 * @Date: 2021/6/22 10:18 上午
 */
public interface MetaDataConstant {
    /**
     * 灰度决策前缀
     */
    String GRAY_DEFINITION_PREFIX = "defense.gray";
    /**
     * 灰度决策名称
     */
    String GRAY_DEFINITION_NAME = "name";
    /**
     * 灰度决策基本信息
     */
    String GRAY_DEFINITION_INFOS = "infos";
}
