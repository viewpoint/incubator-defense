package com.github.defense.plugin.gray.decision.predicate;


import com.github.defense.plugin.gray.instance.GrayInstance;
import com.github.defense.plugin.gray.request.GrayRequest;

import java.util.Collection;
import java.util.List;

/**
 * 决策断言 真正调用factory
 * @Author: lettger
 * @Date: 2021/6/22 12:42 下午
 */
public interface PolicyPredicate {

    /**
     * 灰度决策匹配
     * @param definitions
     * @param request
     * @return
     */
    List<GrayInstance> test(Collection<PolicyDefinition> definitions, GrayRequest request);
}
