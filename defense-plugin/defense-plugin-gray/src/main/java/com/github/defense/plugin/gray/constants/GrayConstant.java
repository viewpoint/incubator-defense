package com.github.defense.plugin.gray.constants;

/**
 * 灰度常量
 * @Author: lettger
 * @Date: 2021/6/6 10:04 下午
 */
public interface GrayConstant {

    String ZUUL_REQUEST = "zuul.request";
    String ZUUL_CONTEXT = "zuul.context";

    String GATEWAY_REQUEST = "gateway.request";

    String REST_REQUEST = "rest.request";
    String FEIGN_REQUEST = "feign.request";

}
