package com.github.defense.plugin.gray.decision.factory;

import com.github.defense.plugin.gray.decision.GrayDecision;
import com.github.defense.plugin.gray.request.GrayHttpRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 比对http url参数
 * @Author: lettger
 * @Date: 2021/6/23 12:09 下午
 */
@Slf4j
public class ParameterGrayDecisionFactory extends AbstractGrayDecisionFactory<ParameterGrayDecisionFactory.Config> {

    public ParameterGrayDecisionFactory() {
        super(Config.class);
    }

    @Override
    public GrayDecision apply(Config config) {
        return args -> {
            log.debug("Parameter Config:{}", config);
            GrayHttpRequest request = (GrayHttpRequest) args;
            List<String> parameter = request.getParameter(config.getName());
            return config.getStrategy().compare(parameter, config.getRegexp());
        };
    }

    @Setter
    @Getter
    @ToString
    public static class Config extends AbstractGrayStrategyFactory.Strategy {
        /**
         * url 后面的参数名称
         */
        private String name;
        /**
         * url 后面参数的值
         */
        private String regexp;
    }
}
