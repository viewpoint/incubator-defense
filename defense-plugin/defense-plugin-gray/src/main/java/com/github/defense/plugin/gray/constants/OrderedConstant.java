package com.github.defense.plugin.gray.constants;

/**
 *
 * 排序 不要设置级别太高，太高会影响到系统中排序 值越小优先级越高
 * @Author: lettger
 * @Date: 2021/6/7 6:06 下午
 */
public interface OrderedConstant {

    int ZUUL_FILTER_ORDERED = 20;
}
