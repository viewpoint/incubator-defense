package com.github.defense.plugin.gray.request;

import lombok.Getter;
import lombok.Setter;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: lettger
 * @Date: 2021/6/6 8:54 下午
 */
public class GrayRequest {

    /**
     * 服务ID
     */
    @Getter
    @Setter
    private String serviceId;

    /**
     * 请求地址
     */
    @Getter
    @Setter
    private URI uri;

    /**
     * 自定义属性
     */
    private Map<String, Object> attributes = new HashMap<>(32);

    public Object getAttribute(String name) {
        return attributes.get(name);
    }

    public void setAttribute(String name, Object value) {
        attributes.put(name, value);
    }

    @Override
    public String toString() {
        return "GrayRequest{" +
                "serviceId='" + serviceId + '\'' +
                ", uri=" + uri +
                ", attributes=" + attributes +
                '}';
    }
}
