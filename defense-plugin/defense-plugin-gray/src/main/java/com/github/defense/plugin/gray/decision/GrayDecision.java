package com.github.defense.plugin.gray.decision;

import com.github.defense.plugin.gray.request.GrayRequest;

/**
 * 灰度决策
 *
 * @Author: lettger
 * @Date: 2021/6/7 10:41 下午
 */
public interface GrayDecision {

    /**
     * 灰度匹配
      * @param args
     * @return
     */
    boolean test(GrayRequest args);
}
