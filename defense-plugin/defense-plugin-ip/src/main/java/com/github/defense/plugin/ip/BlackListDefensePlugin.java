package com.github.defense.plugin.ip;


import com.github.defense.common.request.DefenseRequest;
import com.github.defense.common.response.DefenseResponse;
import com.github.defense.plugin.base.AbstractDefensePlugin;
import com.github.defense.plugin.base.DefensePluginChain;
import lombok.extern.slf4j.Slf4j;

/**
 * 黑名单插件
 * @Author: lettger
 * @Date: 2021/12/9 下午5:57
 */
@Slf4j
public class BlackListDefensePlugin extends AbstractDefensePlugin {
    @Override
    protected DefenseResponse doExecute(DefenseRequest request, DefensePluginChain chain) {
        log.debug("********BlackListPlugin");
        return chain.execute(request);
    }
}
