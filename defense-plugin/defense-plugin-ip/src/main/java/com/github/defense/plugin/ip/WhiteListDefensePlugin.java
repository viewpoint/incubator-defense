package com.github.defense.plugin.ip;

import com.github.defense.common.request.DefenseRequest;
import com.github.defense.common.response.DefenseResponse;
import com.github.defense.plugin.base.AbstractDefensePlugin;
import com.github.defense.plugin.base.DefensePluginChain;
import lombok.extern.slf4j.Slf4j;

/**
 * 白名单插件
 * @Author: lettger
 * @Date: 2021/12/9 下午5:57
 */
@Slf4j
public class WhiteListDefensePlugin extends AbstractDefensePlugin {

    @Override
    protected DefenseResponse doExecute(DefenseRequest request, DefensePluginChain chain) {
        log.debug("********WhiteListPlugin");


        String ip = request.getIp();


        return chain.execute(request);
    }
}
