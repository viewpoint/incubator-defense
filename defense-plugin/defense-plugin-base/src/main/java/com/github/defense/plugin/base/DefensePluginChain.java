package com.github.defense.plugin.base;

import com.github.defense.common.request.DefenseRequest;
import com.github.defense.common.response.DefenseResponse;

/**
 * 插件链 责任链模式 交给spring来管理
 * @Author: lettger
 * @Date: 2021/11/22 下午3:18
 */
public interface DefensePluginChain {

    /**
     * 委托给下一个插件执行
     * @param request
     * @return
     */
    DefenseResponse execute(DefenseRequest request);
}
