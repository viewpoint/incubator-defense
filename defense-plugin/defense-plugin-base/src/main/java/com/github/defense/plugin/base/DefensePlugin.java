package com.github.defense.plugin.base;

import com.github.defense.common.request.DefenseRequest;
import com.github.defense.common.response.DefenseResponse;
import com.github.defense.common.utils.NameUtils;

/**
 * 插件接口
 * 后续所有插件都实现次接口来实现
 * @Author: lettger
 * @Date: 2021/11/22 下午3:14
 */
public interface DefensePlugin {

    /**
     * 处理当前请求后并执行下一个
     * @param request
     * @param chain
     * @return
     */
    DefenseResponse execute(DefenseRequest request, DefensePluginChain chain);

    /**
     * 插件名称
     * @return
     */
    default String name(){
        return NameUtils.normalizeName(getClass(),DefensePlugin.class);
    }

    /**
     * 排序 默认最后执行
     * @return
     */
    default int getOrder(){
        return 0;
    }

    /**
     * 是否执行
     * 返回true 跳过
     * @param request
     * @return
     */
    default boolean skip(DefenseRequest request){
        return false;
    }
}
