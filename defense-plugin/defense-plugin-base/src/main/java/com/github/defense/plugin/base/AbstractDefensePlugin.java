package com.github.defense.plugin.base;

import com.github.defense.common.dto.Plugin;
import com.github.defense.common.request.DefenseRequest;
import com.github.defense.common.response.DefenseResponse;
import com.github.defense.sync.data.api.cache.CacheData;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * 抽象插件扩张 后续插件请继承此插件
 * @Author: lettger
 * @Date: 2021/11/23 下午1:18
 */
@Slf4j
public abstract class AbstractDefensePlugin implements DefensePlugin{

    /**
     * 抽象处理逻辑
     * @param request
     * @param chain
     * @return
     */
    protected abstract DefenseResponse doExecute(DefenseRequest request, DefensePluginChain chain);

    @Override
    public DefenseResponse execute(DefenseRequest request, DefensePluginChain chain) {
        String name = name();
        Plugin plugin = CacheData.getInstance().obtainPlugin(name);
        if(Objects.nonNull(plugin) && plugin.getEnabled()){
            // 处理插件逻辑
            return  doExecute(request,chain);
        }
        return chain.execute(request);
    }
}
