package com.github.defense.starter.common.configuration;

import com.github.defense.common.config.DefenseConfig;
import com.github.defense.plugin.base.subscriber.DefaultPluginSubscriber;
import com.github.defense.starter.common.listener.DefenseEventListener;
import com.github.defense.sync.data.api.ClientRegisterDefenseRepository;
import com.github.defense.sync.data.api.PluginSubscriber;
import com.github.defense.sync.data.http.HttpClientRegisterDefenseRepository;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * defense 配置信息
 * @Author: lettger
 * @Date: 2021/12/2 下午1:14
 */
@Configuration
public class DefenseCommonAutoConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "defense.config", ignoreInvalidFields = true)
    public DefenseConfig defenseConfig() {
        return new DefenseConfig();
    }

    @Bean
    public ClientRegisterDefenseRepository clientRegisterDefenseRepository(final DefenseConfig config){
        return new HttpClientRegisterDefenseRepository(config);
    }


    @Bean
    public DefenseEventListener defenseEventListener(final ClientRegisterDefenseRepository clientRegisterDefenseRepository) {
        return new DefenseEventListener(clientRegisterDefenseRepository);
    }

    /**
     * 处理同步数据
     * @return
     */
    @Bean
    public PluginSubscriber pluginSubscriber(){
        return new DefaultPluginSubscriber();
    }
}
