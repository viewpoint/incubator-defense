package com.github.defense.starter.common.event;

import com.github.defense.common.enums.SceneEnum;
import org.springframework.context.ApplicationEvent;

/**
 * @Author: lettger
 * @Date: 2021/12/3 上午9:44
 */
public class DefenseEvent extends ApplicationEvent {

    private Object data;

    private SceneEnum sceneEnum;

    public DefenseEvent(Object source, Object data, SceneEnum sceneEnum) {
        super(source);
        this.data = data;
        this.sceneEnum = sceneEnum;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public SceneEnum getSceneEnum() {
        return sceneEnum;
    }

    public void setSceneEnum(SceneEnum sceneEnum) {
        this.sceneEnum = sceneEnum;
    }
}
