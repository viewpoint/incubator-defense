package com.github.defense.starter.common.listener;

import com.github.defense.common.enums.SceneEnum;
import com.github.defense.starter.common.event.DefenseEvent;
import com.github.defense.sync.data.api.ClientRegisterDefenseRepository;
import org.springframework.context.ApplicationListener;

/**
 * @Author: lettger
 * @Date: 2021/12/3 上午11:12
 */
public class DefenseEventListener implements ApplicationListener<DefenseEvent> {

    private ClientRegisterDefenseRepository clientRegisterDefenseRepository;

    public DefenseEventListener(final ClientRegisterDefenseRepository clientRegisterDefenseRepository) {
        this.clientRegisterDefenseRepository = clientRegisterDefenseRepository;
    }


    @Override
    public void onApplicationEvent(DefenseEvent event) {
        if (event.getSceneEnum().equals(SceneEnum.APP)
                || event.getSceneEnum().equals(SceneEnum.GATEWAY)
        ) {
            clientRegisterDefenseRepository.register(event.getData());
        }
    }
}
