package com.github.defense.starter.plugin;

import com.github.defense.plugin.base.DefensePlugin;
import com.github.defense.plugin.ip.BlackListDefensePlugin;
import com.github.defense.plugin.ip.WhiteListDefensePlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 插件配置
 * @Author: lettger
 * @Date: 2021/12/11 下午1:46
 */
@Configuration
public class PluginAutoConfiguration {

    @Bean
    public DefensePlugin blackListPlugin(){
        return new BlackListDefensePlugin();
    }

    @Bean
    public DefensePlugin whiteListPlugin(){
        return new WhiteListDefensePlugin();
    }
}
