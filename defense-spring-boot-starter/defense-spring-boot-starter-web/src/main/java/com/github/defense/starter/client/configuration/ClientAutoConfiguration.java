package com.github.defense.starter.client.configuration;

import com.github.defense.common.config.DefenseConfig;
import com.github.defense.starter.client.launch.RequestMappingProcessor;
import com.github.defense.starter.common.configuration.DefenseCommonAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @Author: lettger
 * @Date: 2021/12/2 下午6:41
 */
@Configuration
@ImportAutoConfiguration(DefenseCommonAutoConfiguration.class)
public class ClientAutoConfiguration {

    @Bean
    public RequestMappingProcessor requestMappingProcessor(final DefenseConfig config, final Environment environment, final ApplicationEventPublisher applicationEventPublisher) {
        return new RequestMappingProcessor(config, environment, applicationEventPublisher);
    }
}
