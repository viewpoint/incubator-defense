package com.github.defense.common.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: lettger
 * @Date: 2021/12/13 下午1:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> implements Serializable {
    private static final long serialVersionUID = -7596901707938266753L;

    private static final String OK = "0";
    private static final String ERR = "-1";
    public static final String OK_MESSAGE = "请求成功";
    public static final String ERR_MESSAGE = "请求失败";

    private String code;

    private String message;

    private T data;

    public Result<T> setCode(String code) {
        this.code = code;
        return this;
    }

    public Result<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public Result<T> setData(T data) {
        this.data = data;
        return this;
    }

    public static <T> Result<T> ok(T obj) {
        return new Result<T>().setCode(OK).setMessage(OK_MESSAGE).setData(obj);
    }

    public static <T> Result<T> ok() {
        return new Result<T>().setCode(OK).setMessage(OK_MESSAGE).setData(null);
    }

    public static <T> Result<T> ok(ResponseMessage responseMessage) {
        return new Result<T>().setCode(OK).setMessage(responseMessage.getMessage()).setData(null);
    }

    public static <T> Result<T> err(ResponseMessage responseMessage) {
        return new Result<T>().setCode(responseMessage.getCode()).setMessage(responseMessage.getMessage()).setData(null);
    }

    public static <T> Result<T> err() {
        return new Result<T>().setCode(ERR).setMessage(ERR_MESSAGE).setData(null);
    }

}
