package com.github.defense.common.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

/**
 * 项目路由信息
 * @Author: lettger
 * @Date: 2021/12/2 上午10:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Router {

    /**
     * 接口地址
     * url=/role/{id}, url={/index, /""} ，可以是多个
     */
    private Set<String> urls = new HashSet<>();

    /**
     * get，post，put，delete.一个接口，可以有多个，譬如可以同时接收get和post
     */
    private Set<String> actions;

    /**
     * 接口描述
     */
    private String desc;


}
