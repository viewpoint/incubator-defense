package com.github.defense.common.request;

import lombok.Data;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * 默认请求体
 *
 * @Author: lettger
 * @Date: 2021/12/11 下午4:29
 */
@Data
public class DefenseRequest {

    /**
     * 请求地址
     */
    private URI uri;

    /**
     * 请求方法
     */
    private String method;

    /**
     * 请求的真实IP
     */
    private String ip;

    /**
     * header 参数 包括 cookie;token,自定义的header参数
     */
    private Map<String, List<String>> headers = new LinkedMultiValueMap<>();
    /**
     * 请求参数
     */
    private Map<String, List<String>> params = new LinkedMultiValueMap<>();

    public String getHeader(String name) {
        List<String> headers = this.getHeaders(name);
        if (!CollectionUtils.isEmpty(headers)) {
            return headers.get(0);
        }
        return "";
    }

    public List<String> getHeaders(String name) {
        return this.headers.get(name);
    }

    public String getParam(String name) {
        List<String> params = this.getParams(name);
        if (!CollectionUtils.isEmpty(params)) {
            return params.get(0);
        }
        return "";
    }

    public List<String> getParams(String name) {
        return this.params.get(name);
    }
}
