package com.github.defense.common.response;

/**
 * @Author: lettger
 * @Date: 2021/12/13 下午1:28
 */
public interface ResponseMessage {
    /**
     * 编码
     * @return
     */
    String getCode();

    /**
     * 消息提醒
     * @return
     */
    String getMessage();
}
