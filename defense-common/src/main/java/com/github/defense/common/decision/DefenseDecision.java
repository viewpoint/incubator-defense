package com.github.defense.common.decision;

/**
 *  系统决策者 主要用来对各个规则进行决策判断
 *
 * @Author: lettger
 * @Date: 2021/11/23 下午6:48
 */
public interface DefenseDecision<T> {

    /**
     * 进行数据匹配
     * @param args
     * @return
     */
    boolean test(T args);

}
