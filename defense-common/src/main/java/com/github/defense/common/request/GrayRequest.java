package com.github.defense.common.request;

import lombok.Data;

/**
 * 灰度请求体
 * @Author: lettger
 * @Date: 2021/12/13 下午2:12
 */
@Data
public class GrayRequest extends DefenseRequest{

    /**
     * 服务ID
     */
    private String serviceId;
}
