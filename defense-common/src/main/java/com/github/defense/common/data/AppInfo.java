package com.github.defense.common.data;

import com.github.defense.common.constant.DefaultConstants;
import lombok.*;

import java.util.List;

/**
 * 应用信息
 *
 * @Author: lettger
 * @Date: 2021/12/2 上午10:12
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AppInfo {

    /**
     * 当前应用名称
     */
    private String name;
    /**
     * 服务端口
     */
    private String port;

    /**
     * 当前主机IP
     */
    private String host;

    /**
     * 分组
     */
    private String group = DefaultConstants.APP_DEFAULT_GROUP;

    /**
     * 版本号
     */
    private String version = DefaultConstants.APP_DEFAULT_VERSION;

    /**
     * 项目路由信息
     */
    private List<Router> routers;

    /**
     * 当前项目是否是网关
     */
    private boolean isGateway = false;

}
