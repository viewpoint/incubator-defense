package com.github.defense.common.constant;

/**
 * Nacos setting Constant
 *
 * @Author: lettger
 * @Date: 2021/11/24 上午9:39
 */
public final class DefaultConstants {

    /**
     * Nacos config default group.
     */
    public static final String NACOS_DEFAULT_GROUP = "DEFAULT_GROUP";

    /**
     * 插件在网关的开启和停用
     * nacos 网关插件json
     */
    public static final String NACOS_PLUGIN_DATA_ID = "defense.plugin.json";

    /**
     * 各应用信息 如灰度信息
     */
    public static final String NACOS_APP_DATA_ID = "defense.app.json";

    /**
     * 网关配置
     */
    public static final String NACOS_GATEWAY_DATA_ID = "defense.gateway.json";

    /**
     * 所有服务的默认分组
     */
    public static final String APP_DEFAULT_GROUP = "default";

    /**
     * 所有服务的默认版本号
     */
    public static final String APP_DEFAULT_VERSION = "1.0.0";

    /**
     * nacos 的默认命名空间
     */
    public static final String APP_DEFAULT_NAMESPACE = "public";
}
