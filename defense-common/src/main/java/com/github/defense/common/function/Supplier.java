package com.github.defense.common.function;

import java.io.IOException;

/**
 * 自定义function
 * @Author: lettger
 * @Date: 2021/6/19 6:23 下午
 */
@FunctionalInterface
public interface Supplier<T> {

    /**
     * 自定义function 并返回对象
     * @return
     * @throws IOException
     */
    T get() throws IOException;
}
