package com.github.defense.common.enums;

import com.github.defense.common.exception.DefenseException;

import java.util.Arrays;
import java.util.Objects;

/**
 * @Author: lettger
 * @Date: 2022/1/13 10:40
 */
public enum DataEventTypeEnum {

    /**
     * delete event.
     */
    DELETE,

    /**
     * insert event.
     */
    CREATE,

    /**
     * update event.
     */
    UPDATE,

    /**
     * REFRESH data event type enum.
     */
    REFRESH,

    /**
     * Myself data event type enum.
     */
    MYSELF;

    /**
     * Acquire by name data event type enum.
     *
     * @param name the name
     * @return the data event type enum
     */
    public static DataEventTypeEnum acquireByName(final String name) {
        return Arrays.stream(DataEventTypeEnum.values())
                .filter(e -> Objects.equals(e.name(), name))
                .findFirst()
                .orElseThrow(() -> new DefenseException(String.format(" this DataEventTypeEnum can not support %s", name)));
    }
}
