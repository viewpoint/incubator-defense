package com.github.defense.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @Author: lettger
 * @Date: 2021/12/3 上午9:52
 */
@AllArgsConstructor
@Getter
public enum SceneEnum {


    APP("app"),
    GATEWAY("gateway"),
    ;

    private String name;

    public static SceneEnum byName(final String name) {
        return Arrays.stream(SceneEnum.values())
                .filter(e -> e.getName().equals(name)).findFirst()
                .orElse(APP);
    }

}
