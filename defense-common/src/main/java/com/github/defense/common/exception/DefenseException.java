package com.github.defense.common.exception;

/**
 * @Author: lettger
 * @Date: 2021/12/6 下午1:03
 */
public class DefenseException extends RuntimeException{

    public DefenseException(String message) {
        super(message);
    }
}
