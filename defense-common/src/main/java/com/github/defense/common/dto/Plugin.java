package com.github.defense.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: lettger
 * @Date: 2021/11/23 下午1:45
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Plugin {

    /**
     * 插件ID
     */
    private String id;

    /**
     * 插件名称
     */
    private String name;

    /**
     * redis key 用于存redis数据
     */
    private String key;
    /**
     * 是否启用
     */
    private Boolean enabled;
}
