package com.github.defense.common.config;

import com.github.defense.common.constant.DefaultConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Properties;

/**
 * @Author: lettger
 * @Date: 2021/12/2 下午12:26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public final class DefenseConfig {
    /**
     * 是否是网关
     */
    private boolean gateway = false;

    /**
     * 分组
     */
    private String group = DefaultConstants.APP_DEFAULT_GROUP;


    /**
     * defense 后台地址 多个以逗号隔开
     */
    private String adminAddr;

    /**
     * 扩展参数
     */
    private Properties props = new Properties();

    public DefenseConfig(boolean gateway, String group, String adminAddr) {
        this.gateway = gateway;
        this.group = group;
        this.adminAddr = adminAddr;
    }
}
