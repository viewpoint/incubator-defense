package com.github.defense.common.utils;

/**
 *
 * 获取当前插件的前缀名称
 * @Author: lettger
 * @Date: 2021/11/23 下午1:11
 */
public class NameUtils {

    private NameUtils() {
        throw new AssertionError("Must not instantiate utility class.");
    }

    public static <T> String normalizeName(Class<? extends T> clazz, Class<T> cls) {
        return clazz.getSimpleName().replace(cls.getSimpleName(), "");
    }

}
