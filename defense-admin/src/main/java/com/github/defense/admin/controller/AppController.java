package com.github.defense.admin.controller;

import com.github.defense.common.data.AppInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: lettger
 * @Date: 2021/12/2 下午6:54
 */
@RestController
@RequestMapping("/admin/application")
@Slf4j
public class AppController {

    @PostMapping("/register")
    public void register(@RequestBody AppInfo appInfo){
      // TODO 如果当前服务是网关 启动一个延迟任务 60s向Nacos发送一次请求 保证新启动的服务能够同步到最新的配置

        log.info(appInfo.toString());
    }
}
