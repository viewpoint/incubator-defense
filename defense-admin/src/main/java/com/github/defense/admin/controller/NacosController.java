package com.github.defense.admin.controller;

import cn.hutool.http.HttpUtil;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: lettger
 * @Date: 2021/11/24 下午12:31
 */
@RestController
public class NacosController {


/*    @Autowired
    private ConfigService configService;*/

/*    @NacosInjected
    private ConfigService configService;*/


    public static final String url = "http://127.0.0.1:8848";

    @GetMapping("/create")
    public Object createConfig(String dataId) throws NacosException {
/*
         boolean result = configService.publishConfig("defense", NacosConstants.DEFAULT_GROUP,"haha="+info);
*/

        String res = HttpUtil.get(url + "/nacos/v1/cs/configs?dataId="+dataId+"&group=DEFAULT_GROUP&tenant=agent_dev");

         return res;
    }

    @GetMapping("/update")
    public Object updateConfig(String dataId,String content) throws NacosException {
/*
         boolean result = configService.publishConfig("defense", NacosConstants.DEFAULT_GROUP,"haha="+info);
*/
        String u = url + "/nacos/v1/cs/configs?dataId="+dataId+"&group=DEFAULT_GROUP&tenant=agent_dev&content="+content;
        return HttpUtil.post(u,"");
    }

}
