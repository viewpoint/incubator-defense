/*
package com.github.defense.admin.configuration;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.github.defense.admin.configuration.properties.NacosProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

*/
/**
 * @Author: lettger
 * @Date: 2021/11/24 上午10:43
 *//*

@EnableConfigurationProperties(NacosProperties.class)
@Configuration
public class NacosConfiguration {

    @Bean
    @ConditionalOnMissingBean(ConfigService.class)
    public ConfigService configService(final NacosProperties nacosProperties) throws NacosException {
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.SERVER_ADDR, nacosProperties.getUrl());
        properties.put(PropertyKeyConst.USERNAME, nacosProperties.getUsername());
        properties.put(PropertyKeyConst.PASSWORD, nacosProperties.getPassword());
        properties.put(PropertyKeyConst.NAMESPACE,nacosProperties.getNamespace());
        return NacosFactory.createConfigService(properties);
    }
}
*/
