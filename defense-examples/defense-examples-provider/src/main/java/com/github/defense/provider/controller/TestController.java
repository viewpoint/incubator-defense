package com.github.defense.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: lettger
 * @Date: 2021/12/2 下午7:30
 */
@RestController
public class TestController {

    @ApiOperation("登录")
    @GetMapping("/index")
    public String index(){

        return "HAH";
    }
    @ApiOperation("注册")
    @PostMapping("/index1")
    public void index1(){

    }

    @ApiOperation("退出")
    @RequestMapping("/index2")
    public void index2(){

    }
}
